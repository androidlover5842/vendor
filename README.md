# Proprietary blob for GT-S758X (kyleproxx)

### File info:
  * G310HNXXU0APA1: 
    * /lib/hw/audio.primary.hawaii.so
    * /lib/lib_Samsung_Resampler.so
    * /lib/lib_Samsung_SB_AM_for_ICS_v04004.so
    * /lib/lib_SamsungRec_V03011b.so
    * /lib/libbcm_hp_filter.so
    * /lib/libsamsungRecord.so
    * /lib/libsamsungSoundbooster.so
    * /lib/libtinyalsa.so
    * /lib/libtinyalsa_ext.so
    * /etc/audio_policy.conf
    * /etc/asound.conf
    * /usr/share/alsa/alsa.conf

    * /bin/mfgloader
    * /bin/wlandutservice
    * /etc/wifi/bcmdhd_apsta.bin
    * /etc/wifi/bcmdhd_mfg.bin
    * /etc/wifi/bcmdhd_p2p.bin
    * /etc/wifi/bcmdhd_sta.bin
    * /etc/wifi/nvram_mfg.bin
    * /etc/wifi/nvram_net.bin
    * /etc/wifi/p2p_supplicant_overlay.bin
    * /etc/wifi/wpa_supplicant.bin
    * /etc/wifi/wpa_supplicant_overlay.bin
  * S7582XXUAOJ1:
    * /lib/libbrcm_ril_KYLEPRODS.so
    * /lib/libril_KYLEPRODS.so
  * S7580XXUBOL1:
    * The rest

### Other resource:
  - Device tree (GT-S7580): https://github.com/SandPox/android_device_samsung_kylepro
  - Device tree (GT-S7582): https://github.com/SandPox/android_device_samsung_kyleprods
  - Kernel source: https://github.com/SandPox/android_kernel_samsung_kyleproxx

### Credits (Sort by alphabetical order):
  - Pawitp
  - The CyanogenMod Team
  - Zim555